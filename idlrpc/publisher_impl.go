package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/errors"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/log"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

type (
	subInfo struct {
		subId   string               //调用唯一id
		proxyId ProxyId              //订阅方管道id
		trans   transport.ITransport //订阅方管道
	}

	subHanlde struct {
		subList []*subInfo
	}

	publisherImpl struct {
		event2SubList map[string]*subHanlde //event name to subList
		subId2event   map[string]string     //sub id to event name
		logger        log.ILogger           // log handle
		wrapper       *stubWrapper          //sub ptr
	}
)

func newPublisher(logger log.ILogger, wrapper *stubWrapper) *publisherImpl {
	return &publisherImpl{
		event2SubList: make(map[string]*subHanlde),
		subId2event:   make(map[string]string),
		logger:        logger,
		wrapper:       wrapper,
	}
}

func (p *publisherImpl) Publish(eventName string, data []byte) error {
	// 检查是否有人订阅事件
	subs, ok := p.event2SubList[eventName]
	if !ok {
		return nil
	}

	if subs == nil {
		//现在还没有人订阅
		p.logger.Debug("no subscriber for event %s yet", eventName)
		return nil
	}

	// 遍历所有的订阅者，检查管道，然后删除
	subLen := len(subs.subList)
	for index := 0; index < subLen; {
		sub := subs.subList[index]
		//有效性检查
		if sub != nil && !sub.trans.IsClose() {
			// 构造发布消息
			msg := protocol.BuildPublish(sub.subId, uint32(sub.proxyId), uint32(len(data)))
			msg.Buffer = data

			//打包二进制 并且发送
			binData, _ := protocol.PackPublishMsg(msg)
			err := sub.trans.Send(binData)
			if err != nil {
				return err
			}
			//下一个
			index++
		} else {
			//进行删除，将当前位置换到末尾, 代替删除
			subs.subList[index] = subs.subList[subLen-1]
			subLen--
			//删除sub id 映射
			if sub != nil {
				delete(p.subId2event, sub.subId)
				//通知 service sub 取消
				p.wrapper.onCancel(sub.subId, eventName)
			}

		}
	}

	//有需要删除的订阅
	if subLen != len(subs.subList) {
		subs.subList = subs.subList[:subLen]
	}
	return nil
}

func (p *publisherImpl) AddSub(subId string, eventName string, data []byte, proxyId ProxyId, trans transport.ITransport) error {
	// 添加一个订阅到管理器
	subs, ok := p.event2SubList[eventName]
	if !ok {
		//监听第一次到来，这里需要添加
		subs = &subHanlde{
			subList: []*subInfo{},
		}
		p.event2SubList[eventName] = subs
	}

	if _, ok := p.subId2event[subId]; ok {
		p.logger.Warn("sub id %s has exists in this publisher", subId)
		return errors.ErrInvalidSubCtx
	}

	// 记录event 到 subID 映射关系
	p.subId2event[subId] = eventName

	// 走到这里了可以添加了
	sub := &subInfo{
		subId:   subId,
		proxyId: proxyId,
		trans:   trans,
	}
	subs.subList = append(subs.subList, sub)

	p.logger.Info("add event %s id %s to publisher ", eventName, subId)

	return p.wrapper.onSub(subId, eventName, data)
}

func (p *publisherImpl) CancelSub(subId string) error {
	// 对端关闭订阅
	event, ok := p.subId2event[subId]
	if !ok {
		// 没有订阅过，或者出现重复删除
		return nil
	}
	//删除 订阅映射
	delete(p.subId2event, subId)

	sh, ok := p.event2SubList[event]
	if !ok {
		return nil
	}

	//找到对应的subId
	for i := 0; i < len(sh.subList); i++ {
		sub := sh.subList[i]
		if sub == nil {
			continue
		}
		if sub.subId == subId {
			sh.subList = append(sh.subList[:i], sh.subList[i+1:]...)
			break
		}
	}
	// 调用回调函数
	return p.wrapper.onCancel(subId, event)
}
