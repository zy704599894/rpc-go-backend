package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/errors"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/log"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
)

type (
	eventHandler struct {
		cbMap       map[string]EventCallBack
		event2SubId map[string]string
		subId2event map[string]string
	}
	//serviceSubCache manage service instance's sub info
	serviceSubCache map[uint32]*eventHandler

	//subscriberImpl handled by proxy
	subscriberImpl struct {
		serviceSubCache serviceSubCache
		logger          log.ILogger
		uuid            IUuidFactory
		owner           IProxy
	}
)

func newEventHandler() *eventHandler {
	return &eventHandler{
		cbMap:       make(map[string]EventCallBack),
		event2SubId: make(map[string]string),
		subId2event: make(map[string]string),
	}
}

func newSubscriber(logger log.ILogger, factory IUuidFactory, owner IProxy) *subscriberImpl {
	return &subscriberImpl{
		serviceSubCache: make(serviceSubCache),
		logger:          logger,
		uuid:            factory,
		owner:           owner,
	}
}

func (s *subscriberImpl) Subscribe(serviceID uint32, eventName string, data []byte, cb EventCallBack) error {
	//判断 连接有效性
	trans := s.owner.GetTransport()
	if trans.IsClose() {
		return errors.ErrProxyInvalid
	}

	var subHandle *eventHandler
	subHandle, ok := s.serviceSubCache[serviceID]

	if !ok {
		//第一次通过proxy 进行订阅
		subHandle = newEventHandler()
		s.serviceSubCache[serviceID] = subHandle
	}

	// 检查重复订阅
	subId, ok := subHandle.event2SubId[eventName]
	if ok {
		s.logger.Warn("service instance %d subscribe event %s repetitive")
		return errors.ErrRepeatedSub
	}

	// 生成uid
	subId = s.uuid.NewUuid()
	// 添加映射
	subHandle.event2SubId[eventName] = subId
	subHandle.subId2event[subId] = eventName

	//添加方法回调
	subHandle.cbMap[eventName] = cb

	// 构造协议
	msg := protocol.BuildSubscribe(subId, uint32(s.owner.GetID()), s.owner.GetUUID(), s.owner.GetTargetID(), len(eventName), len(data))
	data = append([]byte(eventName), data...)
	msg.Buffer = data

	// 打包协议
	binary, _ := protocol.PackSubscribeMsg(msg)
	err := trans.Send(binary)
	if err != nil {
		return err
	}

	return nil
}

func (s *subscriberImpl) OnNotify(subId string, bytes []byte) error {
	//广播所有订阅了的服务
	for _, v := range s.serviceSubCache {
		if event, ok := v.subId2event[subId]; ok {
			cb := v.cbMap[event]
			cb(event, bytes)
		}
	}
	return nil
}

func (s *subscriberImpl) Cancel(serviceID uint32, event string) error {
	//判断 连接有效性
	trans := s.owner.GetTransport()
	if trans.IsClose() {
		return errors.ErrProxyInvalid
	}
	var subHandle *eventHandler
	subHandle, ok := s.serviceSubCache[serviceID]

	if !ok {
		//关闭的时候如果没有则视为错误
		return errors.ErrInvalidSubCtx
	}

	subId := subHandle.event2SubId[event]
	//构造关闭包
	msg := protocol.BuildCancel(subId)
	//打包协议
	binData, _ := protocol.PackCancelMsg(msg)
	err := trans.Send(binData)
	if err != nil {
		return err
	}

	//清理缓存
	delete(subHandle.event2SubId, event)
	delete(subHandle.subId2event, subId)
	delete(subHandle.cbMap, subId)

	return nil
}

func (s *subscriberImpl) CancelAll(serviceID uint32) error {
	//判断 连接有效性
	trans := s.owner.GetTransport()
	if trans.IsClose() {
		return errors.ErrProxyInvalid
	}

	var subHandle *eventHandler
	subHandle, ok := s.serviceSubCache[serviceID]

	if !ok {
		//关闭的时候如果没有则视为错误
		return errors.ErrInvalidSubCtx
	}

	for k := range subHandle.subId2event {
		//构造关闭包
		msg := protocol.BuildCancel(k)
		//打包协议
		binData, _ := protocol.PackCancelMsg(msg)
		err := trans.Send(binData)
		if err != nil {
			return err
		}
	}

	//重置event handle
	s.serviceSubCache[serviceID] = newEventHandler()
	return nil
}
