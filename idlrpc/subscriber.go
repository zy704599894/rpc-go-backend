package idlrpc

type (
	//EventCallBack will be called while subscriber receive sub event
	EventCallBack func(string, []byte)
	//ISubscriber is the manager of sub request which been mount to proxy instance
	ISubscriber interface {
		//Subscribe will send sub request to target service
		Subscribe(uint32, string, []byte, EventCallBack) error
		//OnNotify will be called while publisher pub event
		OnNotify(string, []byte) error
		//Cancel will remove sub info from publisher
		Cancel(uint32, string) error
		//CancelAll will remove all sub info from publisher
		CancelAll(uint32) error
	}
)
