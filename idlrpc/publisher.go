package idlrpc

import "gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"

type (
	IPublisher interface {
		//Publish will broadcast event to all subscriber
		Publish(string, []byte) error
		//AddSub will add subscriber to Service
		AddSub(string, string, []byte, ProxyId, transport.ITransport) error
		//CancelSub will remove subscriber by sub_id
		CancelSub(string) error
	}
)
