package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/log"
)

type (
	Options struct {
		ctx            IServiceContext
		logger         log.ILogger
		uuid           IUuidFactory
		stackTrace     bool
		callTrace      bool
		messageHandler IMessageHandler
	}
	Option func(*Options)
)

func (o *Options) StackTrace() bool {
	return o.stackTrace
}

func (o *Options) CallTrace() bool {
	return o.callTrace
}

func WithServiceContext(ctx IServiceContext) Option {
	return func(o *Options) {
		o.ctx = ctx
	}
}

func WithUuidFactory(uid IUuidFactory) Option {
	return func(o *Options) {
		o.uuid = uid
	}
}

func WithMessageHandler(handler IMessageHandler) Option {
	return func(o *Options) {
		o.messageHandler = handler
	}
}

func WithLogger(logger log.ILogger) Option {
	return func(o *Options) {
		o.logger = logger
	}
}

func WithStackTrace(open bool) Option {
	return func(o *Options) {
		o.stackTrace = open
	}
}

func WithCallTrace(open bool) Option {
	return func(o *Options) {
		o.callTrace = open
	}
}
