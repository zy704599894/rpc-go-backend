package example

import (
	"encoding/binary"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

type (
	TestMsgHandler struct {
		RawMsgChan chan *testNoRpcMessage
	}

	testCustomMsgHeader struct {
		MsgId       uint16
		GlobalIndex protocol.GlobalIndexType
	}

	testNoRpcMessage struct {
		header *testCustomMsgHeader
		buffer []byte
	}
)

func NewTestMsgHandler() *TestMsgHandler {
	return &TestMsgHandler{
		RawMsgChan: make(chan *testNoRpcMessage, 100),
	}
}

func (h *TestMsgHandler) OnLoggedOut(index protocol.GlobalIndexType) {
}

func (h *TestMsgHandler) OnIdentityNotify(transport transport.ITransport, identityID string, identityTag string) {
}

func (h *TestMsgHandler) OnRawMessage(transport transport.ITransport, msg []byte) {

	headerLen := binary.Size(testCustomMsgHeader{})

	customMsg := &testNoRpcMessage{
		header: &testCustomMsgHeader{},
		buffer: msg[headerLen:],
	}

	protocol.ParseProtocolHeader(msg[:headerLen], customMsg.header)

	h.RawMsgChan <- customMsg
}
