package example

import (
	"context"
	"crypto/rand"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/example/pbdata"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/internal/logger"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
	"google.golang.org/protobuf/proto"
)

type (
	testApp struct {
		rpc        idlrpc.IRpc
		cancel     chan bool
		wg         sync.WaitGroup
		msgHandler *TestMsgHandler
	}

	testUuidGenerator struct {
	}
)

var (
	implCaller   *TestCallerImpl
	callDelegate reflect.Value
)

func init() {
	implCaller = NewTestCaller()
	tInfo := reflect.TypeOf(implCaller)
	method, ok := tInfo.MethodByName("GetInfo")
	if ok {
		callDelegate = method.Func
	}
}

func (uid *testUuidGenerator) NewUuid() string {
	//return fmt.Sprintf("%16d", time.Now().Unix())
	key := make([]byte, 16)
	_, err := rand.Read(key)
	if err != nil {
		return ""
	}
	return hex.EncodeToString(key[:])
}

func (t *testApp) init() error {
	t.cancel = make(chan bool)
	t.wg = sync.WaitGroup{}

	t.msgHandler = NewTestMsgHandler()
	t.rpc = idlrpc.CreateRpcFramework()
	err := t.rpc.Init(
		idlrpc.WithLogger(&logger.DefaultLogger{}),
		idlrpc.WithStackTrace(true),
		idlrpc.WithUuidFactory(&testUuidGenerator{}),
		idlrpc.WithMessageHandler(t.msgHandler),
	)
	return err
}

func (t *testApp) start() {
	_ = t.rpc.Start()
	go func() {
		t.wg.Add(1)
		defer t.wg.Done()

		timer := time.NewTimer(10 * time.Millisecond)
		for {
			select {
			case <-timer.C:
				t.rpc.Tick()
				timer.Reset(10 * time.Millisecond)
			case <-t.cancel:
				return
			}
		}
	}()
}

func (t *testApp) stop() {
	t.cancel <- true
	t.wg.Wait()
	_ = t.rpc.ShutDown()
}

func TestRpcInit(t *testing.T) {
	app := testApp{}
	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	app.start()
	time.Sleep(1 * time.Second)
	app.stop()
}

func TestAddService(t *testing.T) {
	app := testApp{}
	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}
	app.start()
	if err := app.rpc.RegisterService(NewTestCaller()); err != nil {
		t.Fatal(err)
	}
	time.Sleep(1 * time.Second)
	app.stop()
}

func TestTestCallerStub_AddSub(t *testing.T) {
	// 测试订阅
	app := testApp{}
	trans := NewTransportRing()
	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}

	if err := app.rpc.AddProxyCreator(SrvUUID, TestCallerProxyCreator); err != nil {
		t.Fatal(err)
	}

	app.start()
	testService := NewTestCaller()
	if err := app.rpc.RegisterService(testService); err != nil {
		t.Fatal(err)
	}

	//获取proxy
	proxy, err := app.rpc.GetServiceProxy(SrvUUID, trans)
	if err != nil {
		t.Fatalf("get proxy error %s", err.Error())
	}
	cb := func(event string, data []byte) {}
	err = proxy.Subscribe("testevent", nil, cb)
	if err != nil {
		t.Fatalf("Subscribe error %s", err.Error())
	}

	mesg := trans.PopSend()
	trans.Write(mesg, 0)

	ctx := NewDemoServiceContext()
	ctx.trans = trans

	app.rpc.OnMessage(ctx)

	time.Sleep(100 * time.Millisecond)

	if len(testService.subInfo) != 1 {
		t.Fatalf("unexpected sub info length %d! ", len(testService.subInfo))
	}

	if testService.subInfo[0].event != "testevent" {
		t.Fatalf("unexpected sub event info %s", testService.subInfo[0].event)
	}

	app.stop()
}

func TestTestCallerStub_Pub(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()
	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}

	if err := app.rpc.AddProxyCreator(SrvUUID, TestCallerProxyCreator); err != nil {
		t.Fatal(err)
	}

	app.start()
	testService := NewTestCaller()
	if err := app.rpc.RegisterService(testService); err != nil {
		t.Fatal(err)
	}

	//获取proxy
	proxy, err := app.rpc.GetServiceProxy(SrvUUID, trans)
	if err != nil {
		t.Fatalf("get proxy error %s", err.Error())
	}
	beNotified := false
	cb := func(event string, data []byte) {
		if event != "testevent" {
			t.Fatalf("unexpected subscribe event name %s", event)
		}

		if string(data) != "hello plato" {
			t.Fatalf("unexpected subscribe event data %s", string(data))
		}
		beNotified = true
	}
	err = proxy.Subscribe("testevent", nil, cb)
	if err != nil {
		t.Fatalf("Subscribe error %s", err.Error())
	}

	mesg := trans.PopSend()
	trans.Write(mesg, 0)

	ctx := NewDemoServiceContext()
	ctx.trans = trans

	app.rpc.OnMessage(ctx)

	time.Sleep(100 * time.Millisecond)

	// publish
	app.rpc.Publish(SrvUUID, 0, "testevent", []byte("hello plato"))
	mesg = trans.PopSend()
	trans.Write(mesg, 0)

	app.rpc.OnMessage(ctx)

	if !beNotified {
		t.Fatalf("not trigger event callback")
	}

	app.stop()

}

func TestTestCallerImpl_OnCancel(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()
	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}

	if err := app.rpc.AddProxyCreator(SrvUUID, TestCallerProxyCreator); err != nil {
		t.Fatal(err)
	}

	app.start()
	testService := NewTestCaller()
	if err := app.rpc.RegisterService(testService); err != nil {
		t.Fatal(err)
	}

	//获取proxy
	proxy, err := app.rpc.GetServiceProxy(SrvUUID, trans)
	if err != nil {
		t.Fatalf("get proxy error %s", err.Error())
	}

	cb := func(event string, data []byte) {
		t.Fatalf("unexcpeted trigger !!!")
	}

	err = proxy.Subscribe("testevent", nil, cb)
	if err != nil {
		t.Fatalf("Subscribe error %s", err.Error())
	}

	mesg := trans.PopSend()
	trans.Write(mesg, 0)

	ctx := NewDemoServiceContext()
	ctx.trans = trans

	app.rpc.OnMessage(ctx)
	time.Sleep(100 * time.Millisecond)

	if err := proxy.Cancel("testevent"); err != nil {
		t.Fatal(err)
	}

	// 执行一次tick
	mesg = trans.PopSend()
	trans.Write(mesg, 0)
	app.rpc.OnMessage(ctx)

	time.Sleep(100 * time.Millisecond)

	if testService.subInfo != nil {
		t.Fatal("not delete sub info")
	}

	// publish
	app.rpc.Publish(SrvUUID, 0, "testevent", []byte("hello plato"))

	app.stop()
}

func TestSendMessage(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()
	caller := NewTestCaller()

	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}
	app.start()
	if err := app.rpc.RegisterService(caller); err != nil {
		t.Fatal(err)
	}

	pbarg := &pbdata.TestCaller_SetInfoArgs{}
	pbarg.Arg1 = "hello"

	// parameters serialize data, message may be nil
	pkg, err := proto.Marshal(pbarg)
	if err != nil {
		return
	}
	// wrapper rpc call request
	reqPb := &protocol.RequestPackage{
		Header: &protocol.RpcCallHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.CallHeadSize + len(pkg)),
				Type:   protocol.RequestMsg,
			},
			ServiceUUID: SrvUUID,
			ServerID:    1,
			CallID:      1,
			MethodID:    1,
		},
		Buffer: pkg,
	}

	reqData, _ := protocol.PackReqMsg(reqPb)
	_, _ = trans.Write(reqData, len(reqData))

	ctx := NewDemoServiceContext()
	ctx.trans = trans

	_ = app.rpc.OnMessage(ctx)
	time.Sleep(2 * time.Second)
	if caller.name != "hello" {
		t.Error("call method1 failed! ")
	}
	app.stop()
}

func TestPeerPanic(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()
	caller := NewTestCaller()

	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddStubCreator(SrvUUID, TestCallerStubCreator); err != nil {
		t.Fatal(err)
	}
	app.start()
	if err := app.rpc.RegisterService(caller); err != nil {
		t.Fatal(err)
	}

	pbarg := &pbdata.TestCaller_GetInfoArgs{}

	// parameters serialize data, message may be nil
	pkg, err := proto.Marshal(pbarg)
	if err != nil {
		return
	}
	// wrapper rpc call request
	reqPb := &protocol.RequestPackage{
		Header: &protocol.RpcCallHeader{
			RpcMsgHeader: protocol.RpcMsgHeader{
				Length: uint32(protocol.CallHeadSize + len(pkg)),
				Type:   protocol.RequestMsg,
			},
			ServiceUUID: SrvUUID,
			ServerID:    1,
			CallID:      1,
			MethodID:    2,
		},
		Buffer: pkg,
	}

	reqData, _ := protocol.PackReqMsg(reqPb)
	_, _ = trans.Write(reqData, len(reqData))
	ctx := NewDemoServiceContext()
	ctx.trans = trans

	_ = app.rpc.OnMessage(ctx)
	time.Sleep(2 * time.Second)
	app.stop()
}

func TestRef(t *testing.T) {
	service := NewTestCaller()
	tInfo := reflect.TypeOf(service)
	for i := 0; i < tInfo.NumMethod(); i++ {
		m := tInfo.Method(i)
		fmt.Println(m.Name)
	}
}

func BenchmarkTestCallerImpl_GetInfo(b *testing.B) {
	//ctx := context.Background()
	var sh idlrpc.IService
	sh = implCaller
	//for n := 0; n < b.N; n++ {
	//	//ctx = context.WithValue(ctx, "text", 1)
	//	implCaller.GetNickName()
	//}
	//b.ResetTimer()
	for n := 0; n < b.N; n++ {
		sh.GetNickName()
	}
}

func BenchmarkContext(b *testing.B) {
	type testKey struct{}
	ctx := context.Background()
	ctx = context.WithValue(ctx, testKey{}, 1234647)
	for n := 0; n < b.N; n++ {
		id := ctx.Value(testKey{})
		if _, ok := id.(int); !ok {
			b.Fatal("convert interface ")
		}
	}
}

func TestRetryRpcCall(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()
	//caller := NewTestCaller()

	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	if err := app.rpc.AddProxyCreator(SrvUUID, TestCallerProxyCreator); err != nil {
		t.Fatal(err)
	}
	app.start()

	// get proxy
	pInterface, err := app.rpc.GetServiceProxy(SrvUUID, trans)
	if err != nil {
		t.Fatal(err)
	}
	sp := pInterface.(*TestCallerProxy)
	if sp == nil {
		t.Fatal("get caller proxy error !!!")
	}

	_, err = sp.GetInfo()
	if err == nil {
		t.Fatalf("unexception error return")
	}

	app.stop()
}

func TestSendRawMsg(t *testing.T) {
	app := testApp{}
	trans := NewTransportRing()

	if err := app.init(); err != nil {
		t.Fatal(err)
	}
	app.start()

	err := testSendRawMsg(trans, 1, []byte("hello"))
	if err != nil {
		t.Fatal(err)
	}

	ctx := NewDemoServiceContext()
	ctx.trans = trans
	trans.Write(trans.PopSend(), -1)

	err = app.rpc.OnMessage(ctx)
	if err != nil {
		t.Fatal(err)
	}

	info := <-app.msgHandler.RawMsgChan
	if info.header.MsgId != 1 {
		t.Fatal("msg id error")
	}
	if string(info.buffer) != "hello" {
		t.Fatal("msg content error")
	}
}

func testSendRawMsg(trans transport.ITransport, msgId uint16, msg []byte) error {
	reqPb := &protocol.NoRpcPackage{
		Header: &protocol.RpcMsgHeader{
			Length: 0,
			Type:   protocol.NotRpcMsg,
		},
		Buffer: nil,
	}

	customMsg := testNoRpcMessage{
		header: &testCustomMsgHeader{
			MsgId:       msgId,
			GlobalIndex: trans.GlobalIndex(),
		},
		buffer: msg,
	}

	reqPb.Header.Length = uint32(binary.Size(customMsg.header) + len(msg) + protocol.RpcHeadSize)

	reqPb.Buffer, _ = protocol.PackPlatoMessage(customMsg.header, customMsg.buffer, len(msg))
	packData, _ := protocol.PackPlatoMessage(reqPb.Header, reqPb.Buffer, len(reqPb.Buffer))

	err := trans.Send(packData)
	if err != nil {
		return err
	}
	return nil
}
