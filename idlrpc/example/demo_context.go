package example

import (
	"fmt"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
	golog "log"
)

type (
	DemoServiceContext struct {
		*idlrpc.RpcContext
		trans  *TransportRing
		header string
	}
)

func NewDemoServiceContext() *DemoServiceContext {
	return &DemoServiceContext{
		RpcContext: &idlrpc.RpcContext{},
	}
}

func (c *DemoServiceContext) Clone() idlrpc.IServiceContext {
	return &DemoServiceContext{
		RpcContext: c.RpcContext.Clone(),
		trans:      c.trans,
	}
}

func (c *DemoServiceContext) GetTransport() transport.ITransport {
	return c.trans
}

func (c *DemoServiceContext) GetIdentityID() string {
	return c.trans.IdentityID()
}

func (c *DemoServiceContext) GetIdentityTag() string {
	return c.trans.IdentityTag()
}

func (c *DemoServiceContext) GetProxyFromPeer(uuid uint64) idlrpc.IProxy {
	//TODO implement me
	panic("implement me")
}

func (c *DemoServiceContext) GetProxy(uuid uint64) idlrpc.IProxy {
	//TODO implement me
	panic("implement me")
}

func (c *DemoServiceContext) GetProxyWithNickname(uint64, string) idlrpc.IProxy {
	//TODO implement me
	panic("implement me")
}

func (c *DemoServiceContext) PushLogHeader(s string) {
	c.header += s
}

func (c *DemoServiceContext) Debug(format string, i ...interface{}) {
	logInfo := fmt.Sprintf(format, i...)
	golog.Print(c.header, logInfo)
}

func (c *DemoServiceContext) Info(format string, i ...interface{}) {
	logInfo := fmt.Sprintf(format, i...)
	golog.Print(c.header, logInfo)
}

func (c *DemoServiceContext) Warning(format string, i ...interface{}) {
	logInfo := fmt.Sprintf(format, i...)
	golog.Print(c.header, logInfo)
}

func (c *DemoServiceContext) Error(format string, i ...interface{}) {
	logInfo := fmt.Sprintf(format, i...)
	golog.Fatal(c.header, logInfo)
}
