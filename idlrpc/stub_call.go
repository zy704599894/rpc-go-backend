package idlrpc

import (
	"fmt"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
)

// CallUuid  stub call uuid generate by manager
type CallUuid uint32

// CallInfo remote call data
type CallInfo struct {
	uuid      CallUuid                 // uuid, generate by manager
	srvUuid   uint64                   // service uuid
	srvInstID uint32                   // service instance id
	callID    uint32                   // proxy call id
	methodID  uint32                   // method id
	oneWay    uint16                   // is one way method
	buffer    []byte                   // serialize body
	globalID  protocol.GlobalIndexType // global index id
	ctx       IServiceContext          // RPC call context
}

// newCallInfo create stub call by manager
func newCallInfo(ctx IServiceContext, req *protocol.RequestPackage, uuid CallUuid) *CallInfo {
	return &CallInfo{
		uuid:      uuid,
		srvUuid:   req.Header.ServiceUUID,
		srvInstID: req.Header.ServerID,
		callID:    req.Header.CallID,
		methodID:  req.Header.MethodID,
		buffer:    req.Buffer,
		ctx:       ctx,
	}
}

func newCallInfoForProxy(ctx IServiceContext, req *protocol.ProxyRequestPackage, uuid CallUuid) *CallInfo {
	return &CallInfo{
		uuid:      uuid,
		srvUuid:   req.Header.ServiceUUID,
		srvInstID: req.Header.ServerID,
		callID:    req.Header.CallID,
		methodID:  req.Header.MethodID,
		globalID:  req.Header.GlobalIndex,
		oneWay:    req.Header.OneWay,
		buffer:    req.Buffer,
		ctx:       ctx,
	}
}

// MethodID rpc method call id
func (sc *CallInfo) MethodID() uint32 {
	return sc.methodID
}

// CallID rpc remote proxyCall call id
func (sc *CallInfo) CallID() uint32 {
	return sc.callID
}

// GetUUID stub call uuid
func (sc *CallInfo) GetUUID() CallUuid {
	return sc.uuid
}

func (sc *CallInfo) GetServiceUUID() uint64 {
	return sc.srvUuid
}

func (sc *CallInfo) GlobalIndex() protocol.GlobalIndexType {
	return sc.globalID
}

func (sc *CallInfo) GetBody() []byte {
	return sc.buffer
}

func (sc *CallInfo) doRet(msg []byte) error {
	trans := sc.ctx.GetTransport()
	if trans.IsClose() {
		//TODO add common error
		sc.ctx.Error("The transport has been finished while rpc call %d return", sc.callID) //log2.Error("[Service] %s,%d,0 service method %s call raise panic %v, but transport has been closed", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), stubCall.MethodID(), r)
		return fmt.Errorf("[Service] End RPC %d:%s call %d exception ", sc.globalID, trans.RemoteAddr(), sc.callID)
	}
	err := trans.Send(msg)
	if err != nil {
		return err
	}
	return nil
}
