// Package idlrpc rpc-backend-go framework interface
package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
	"google.golang.org/protobuf/proto"
)

const (
	RpcNotInit = iota
	RpcRunning
	RpcClosed
)

type (
	IUuidFactory interface {
		//NewUuid will generate string uuid with 32 characters
		NewUuid() string
	}

	IRpc interface {
		Init(...Option) error
		// Start rpc framework,init data and start some goroutine
		Start() error
		// Tick Calls the main loop in the user master coroutine。
		Tick() error
		// ShutDown close rpc framework close
		ShutDown() error
		// Options options get rpc options
		Options() *Options
		// OnMessage deal network message
		OnMessage(ctx IServiceContext) error
		// OnProxyMessage trans proxy message to inside service
		OnProxyMessage(tran transport.ITransport, ph IProxyHandler) error
		// RegisterService register user impl service struct to framework
		RegisterService(service IService) error
		// Call service proxy call remote sync
		//Return resp unmarshalled proto buffer and exec result
		Call(proxyId IProxy, methodId, timeout uint32, retry int32, message proto.Message) ([]byte, error)
		//Publish will notify all subscriber with event's data
		Publish(uuid SvcUuid, serviceID int32, event string, data []byte) error
		// GetProxyFromPeer get proxy by stub call
		GetProxyFromPeer(ctx IServiceContext, uuid uint64) (IProxy, error)
		// GetServiceProxy get service proxy
		GetServiceProxy(uuid uint64, trans transport.ITransport) (IProxy, error)
		// GetExtraProxy get extra service proxy
		GetExtraProxy(uuid uint64, globalIndex protocol.GlobalIndexType, trans transport.ITransport) (IProxy, error)
		// AddProxyCreator add proxy creator
		AddProxyCreator(uuid uint64, pc ProxyCreator) error
		// AddStubCreator add stub creator
		AddStubCreator(uuid uint64, bc StubCreator) error
	}
)
