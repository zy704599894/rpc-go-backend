package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

type (
	IMessageHandler interface {
		OnLoggedOut(index protocol.GlobalIndexType)
		OnIdentityNotify(transport transport.ITransport, identityID string, identityTag string)
		OnRawMessage(transport transport.ITransport, msg []byte)
	}
)
