package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

type (
	IProxyHandler interface {
		OnRelay(trans transport.ITransport, header *protocol.RpcMsgHeader) error
	}
)
