package idlrpc

import (
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

type (
	ContextIterator func(string, string)
	ContextKV       map[string]string
	//IServiceContext The context information needed to communicate between services
	IServiceContext interface {
		GetTransport() transport.ITransport
		// GetGlobalIndex get external connection id
		GetGlobalIndex() protocol.GlobalIndexType
		// GetIdentityID will return the identity id of the source of the RPC call
		GetIdentityID() string
		// GetIdentityTag will return the identity tag of the source of the RPC call
		GetIdentityTag() string
		// GetProxyFromPeer Gets the proxy utility function of the peer service
		GetProxyFromPeer(uuid uint64) IProxy
		// GetProxy get the Proxy for the service
		GetProxy(uuid uint64) IProxy
		// GetProxyWithNickname get the Proxy for the service by nickname
		GetProxyWithNickname(uuid uint64, name string) IProxy
		// GetValue returns the value for the given key, ie: (value, true).
		// If the value does not exist it returns (nil, false)
		GetValue(string) (string, bool)
		// GetRequestContext will return the request context kv map
		GetRequestContext() ContextKV
		// GetResponseValue returns the value for the given key in the response context
		GetResponseValue(string) (string, bool)
		// GetResponseContext will return the response context kv map
		GetResponseContext() ContextKV
		// GetUserContext will return the user context kv map
		GetUserContext() ContextKV
		// SetContext will set the context of the RPC request
		SetContext(string, string)
		// SetUserContext Set the user context to be passed on the next RPC call. Null character will delete the Key.
		SetUserContext(string, string)
		// SetResponseContext Set the response context to be passed on the RPC response.
		SetResponseContext(string, string)
		// Debug Tool function, automatically append call information, print Debug level logs
		Debug(string, ...interface{})
		// Info Tool function, automatically append call information, print INFO level logs
		Info(string, ...interface{})
		// Warning Tool function, automatically append call information, print WARNING level logs
		Warning(string, ...interface{})
		// Error Tool function, automatically append call information, print ERROR level logs
		Error(string, ...interface{})
		// Clone  a new context that does not contain user-defined key values.
		Clone() IServiceContext
		// =============== NoExported Function ===============
		// setGlobalIndex will set the global index of the RPC call
		setGlobalIndex(indexType protocol.GlobalIndexType)
	}

	RpcContext struct {
		requestContext  ContextKV
		responseContext ContextKV
		userContext     ContextKV
		globalIndex     protocol.GlobalIndexType
	}
)

func (c *RpcContext) GetGlobalIndex() protocol.GlobalIndexType {
	return c.globalIndex
}

func (c *RpcContext) setGlobalIndex(index protocol.GlobalIndexType) {
	c.globalIndex = index
}

func (c *RpcContext) SetContext(key string, value string) {
	if c.requestContext == nil {
		c.requestContext = make(ContextKV)
	}

	c.requestContext[key] = value
}

func (c *RpcContext) GetValue(key string) (string, bool) {
	// 没有上下文， 直接返回
	if c.requestContext == nil {
		return "", false
	}

	value, ok := c.requestContext[key]
	return value, ok
}

func (c *RpcContext) GetRequestContext() ContextKV {
	return c.requestContext
}

func (c *RpcContext) SetUserContext(key string, value string) {
	if len(key) == 0 {
		return
	}

	if c.userContext == nil {
		c.userContext = make(ContextKV)
	}

	if len(value) == 0 {
		delete(c.userContext, key)
	} else {
		c.userContext[key] = value
	}
}

func (c *RpcContext) GetUserContext() ContextKV {
	return c.userContext
}

func (c *RpcContext) SetResponseContext(key string, value string) {
	if len(key) == 0 {
		return
	}

	if c.responseContext == nil {
		c.responseContext = make(ContextKV)
	}

	c.responseContext[key] = value
}

func (c *RpcContext) GetResponseValue(key string) (string, bool) {
	if c.responseContext == nil {
		return "", false
	}

	value, ok := c.responseContext[key]
	return value, ok
}

func (c *RpcContext) GetResponseContext() ContextKV {
	return c.responseContext
}

func (c *RpcContext) Clone() *RpcContext {
	return &RpcContext{}
}
