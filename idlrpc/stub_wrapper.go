package idlrpc

import (
	"context"
	"runtime/debug"
	"sync"
	"sync/atomic"

	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/internal/common"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/errors"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/log"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/protocol"
	"gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"
)

// StubCallQueue service remote call queue
type cmdQueue chan rpcTask
type stopSign chan struct{}

// stubWrapper user stub wrapper
type stubWrapper struct {
	isClose   int32           //is this service not service again, 0 not, 1 closed
	srvImp    IStub           //stub interface user implement
	wg        sync.WaitGroup  //worker goroutine waiter
	cmdCache  cmdQueue        //rpc remote cmd queue
	stopCh    stopSign        //stop signal channel, maybe be replaced with context cancel function
	logger    log.ILogger     //logger instance
	ctx       context.Context //graceful close single
	publisher *publisherImpl  //publisher TODO change to publisher interface
}

// newStubWrapper create stub base while service register
func newStubWrapper(impl IStub, logger log.ILogger) *stubWrapper {
	if impl == nil {
		panic("[IStub] register invalid service ")
		return nil
	}

	wrapper := &stubWrapper{
		isClose:  0,
		srvImp:   impl,
		wg:       sync.WaitGroup{},
		cmdCache: make(cmdQueue, common.DefaultCallCache),
		stopCh:   make(stopSign),
		logger:   logger,
		//publisher: newPublisher(logger),
	}

	wrapper.publisher = newPublisher(logger, wrapper)

	return wrapper
}

// Init service, after register
func (s *stubWrapper) init(ctx IServiceContext) (err error) {
	// add recover function to avoid throwing panic in OnTick function
	defer func() {
		if r := recover(); r != nil {
			if s.srvImp == nil {
				//In theory, it will not enter this branch forever
				s.logger.Warn("[Service] 0,0,0 service implement interface is nil !")
				err = errors.ErrServicePanic
				return
			}
			err = errors.NewRpcError(errors.CommErr, "service %s throw panic while executing the initialization function", s.srvImp.GetServiceName())
			s.logger.Warn("[Service] %s,%d,0 service throw exception %v on init ", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), r)
			if stackTrace {
				s.logger.Error("trace back: %s", string(debug.Stack()))
			}
		}
	}()

	if !s.srvImp.OnAfterFork(ctx) {
		err = errors.ErrServiceInit
	}
	return
}

// start start service working loop
func (s *stubWrapper) start() {
	if s.srvImp == nil {
		s.logger.Error("[Service] service implement interface is invalid")
		panic("invalid service")
	}

	num := s.srvImp.GetMultipleNum()
	if num == 0 {
		num = common.DefaultServiceWorker
	}

	//start worker
	for i := uint32(0); i < num; i++ {
		s.wg.Add(1)
		go s.loop()
	}
}

// loop rpc remote call worker, read stub call from message queue
func (s *stubWrapper) loop() {
	defer func() {
		//maybe panic
		if r := recover(); r != nil {
			s.logger.Error("[Service] %s,%d,0 service method call panic!!", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
			if stackTrace {
				s.logger.Error("trace back: %s", string(debug.Stack()))
			}
		}
		s.wg.Done()
	}()

	for {

		// The try-receive operation here is to
		// try to exit the sender goroutine as
		// early as possible. Try-receive and
		// try-send select blocks are specially
		// optimized by the standard Go
		// compiler, so they are very efficient.
		select {
		case <-s.stopCh:
			return
		default:
		}

		select {
		case <-s.stopCh:
			return
		case task := <-s.cmdCache:
			// stub call has been set to nil while shutdown rpc framework
			if task == nil {
				return
			}
			switch t := task.(type) {
			case *rpcCallTask:
				err := s.doCallMethod(t.call)
				//TODO: destroy stub call
				if err != nil {
					//TODO: add record of error code
					return
				}
			case *rpcSubTask:
				s.doAddSub(t)
			case *rpcCancelTask:
				s.doCancelSub(t)
			}

		}
	}
}

func (s *stubWrapper) doCallMethod(stubCall *CallInfo) (err error) {
	if stubCall == nil {
		s.logger.Error("[Service] %s,%d,0 stub call pointer is invalid", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
		return errors.ErrStubCallInvalid
	}
	//recover function, not break loop
	defer func() {
		//recover panic
		if r := recover(); r != nil {
			if !s.srvImp.IsOneWay(stubCall.MethodID()) {
				//build rpc response, notify client exception
				var pkg []byte
				if info, ok := r.(errors.RpcPanicInfo); ok {
					pkg = info.Pkg
				}
				resp := protocol.BuildException(stubCall.CallID(), pkg)
				respData, pkgLen := protocol.PackRespMsg(resp)
				if respData == nil || pkgLen == 0 {
					s.logger.Error("[Service] %s,%d,0 serialize response bytes error !", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
					return
				} else {
					s.logger.Error("[Service] %s,%d,0 runtime error: %v ", s.srvImp.GetServiceName(), stubCall.MethodID(), r)
					if stackTrace {
						s.logger.Error("trace back: %s", string(debug.Stack()))
					}
				}
				err = stubCall.doRet(respData)
				if err != nil {
					return
				}
			}
		}
	}()

	if stubCall.globalID == InvalidGlobalIndex {
		return s.rpcCall(stubCall)
	} else {
		return s.rpcProxyCall(stubCall)
	}
}

func (s *stubWrapper) rpcCall(callInfo *CallInfo) (err error) {

	buffer, err := s.srvImp.Call(callInfo.ctx, callInfo.MethodID(), callInfo.buffer)
	// not one-way function, send response
	if !s.srvImp.IsOneWay(callInfo.MethodID()) {
		execCode := protocol.IDL_SUCCESS
		if err != nil {
			//log error
			s.logger.Warn("[Service] %s,%d,%d method %s call error %v", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), callInfo.CallID(), s.srvImp.GetSignature(callInfo.MethodID()), err)
			//set error
			execCode = protocol.IDL_SERVICE_ERROR
		}
		//Build response package
		resp := &protocol.ResponsePackage{
			Buffer: buffer,
		}
		//srvID compatible with cpp implement, must be zero in golang
		protocol.BuildRespHeader(resp, 0, callInfo.CallID(), execCode)
		respData, pkgLen := protocol.PackRespMsg(resp)
		if respData == nil || pkgLen == 0 {
			s.logger.Error("[Service] %s,%d,0 serialize response bytes error !", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
			err = errors.NewMethodExecError(s.srvImp.GetServiceName(), s.srvImp.GetSignature(callInfo.MethodID()))
			return
		}
		err = callInfo.doRet(respData)
		if err != nil {
			s.logger.Warn("[Service] %s,%d,%d method %s send data error!", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), callInfo.CallID(), s.srvImp.GetSignature(callInfo.MethodID()), err)
			return
		}
	}
	return
}

func (s *stubWrapper) rpcProxyCall(callInfo *CallInfo) (err error) {
	// 设置 调用上下文的必要信息
	callInfo.ctx.setGlobalIndex(callInfo.globalID)
	buffer, err := s.srvImp.Call(callInfo.ctx, callInfo.MethodID(), callInfo.buffer)
	// not one-way function, send response
	if !s.srvImp.IsOneWay(callInfo.MethodID()) {
		execCode := protocol.IDL_SUCCESS
		if err != nil {
			//log error
			s.logger.Warn("[Service] %s,%d,%d method %s exec error %v", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), callInfo.CallID(), s.srvImp.GetSignature(callInfo.MethodID()), err)
			//set error
			execCode = protocol.IDL_SERVICE_ERROR
		}
		//Build response package
		resp := &protocol.ProxyRespPackage{
			Buffer: buffer,
		}
		//srvID compatible with cpp implement, must be zero in golang
		protocol.BuildProxyRespHeader(resp, 0, callInfo.CallID(), execCode, callInfo.globalID)
		respData, pkgLen := protocol.PackProxyRespMsg(resp)
		if respData == nil || pkgLen == 0 {
			s.logger.Error("[Service] %s,%d,0 serialize response bytes error !", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
			err = errors.NewMethodExecError(s.srvImp.GetServiceName(), s.srvImp.GetSignature(callInfo.MethodID()))
			return
		}
		err = callInfo.doRet(respData)
		if err != nil {
			s.logger.Warn("[Service] %s,%d,%d method %s send data error!", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), callInfo.CallID(), s.srvImp.GetSignature(callInfo.MethodID()), err)
			return
		}
	}
	return nil
}

func (s *stubWrapper) doAddSub(task *rpcSubTask) {
	err := s.publisher.AddSub(task.subId, task.eName, task.data, ProxyId(task.proxyId), task.trans)
	if err != nil {
		s.logger.Warn("add subscribe id %s event %s error %v", task.subId, task.eName, task.trans)
		return
	}
}

func (s *stubWrapper) doCancelSub(task *rpcCancelTask) {
	err := s.publisher.CancelSub(task.subId)
	if err != nil {
		s.logger.Warn("cancel subscribe id %s event %s error %v", task.subId)
		return
	}
}

func (s *stubWrapper) onSub(subId string, eventName string, data []byte) error {
	return s.srvImp.OnSub(subId, eventName, data)
}

func (s *stubWrapper) onCancel(subId string, event string) error {
	return s.srvImp.OnCancel(subId, event)
}

func (s *stubWrapper) tick() {
	// add recover function to avoid throwing panic in OnTick function
	defer func() {
		if r := recover(); r != nil {
			if s.srvImp == nil {
				//In theory, it will not enter this branch forever
				s.logger.Warn("[Service] 0,0,0 service implement interface is nil !")
				return
			}
			s.logger.Warn("[Service] %s,%d,0 service throw exception %v on tick ", s.srvImp.GetServiceName(), s.srvImp.GetUUID(), r)
		}
	}()
	//TODO check impl status
	s.srvImp.OnTick()
}

// close close service
func (s *stubWrapper) close() {
	//atomic check close status
	isClose := atomic.LoadInt32(&s.isClose)
	if isClose == 1 {
		s.logger.Warn("[Service] %s,%d,0 stub close multi times", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
		return
	}
	//set close status
	if !atomic.CompareAndSwapInt32(&s.isClose, isClose, 1) {
		// set false, close status have been changed by other goroutine
		s.logger.Info("[Service] %s,%d,0 service has been closed by other goroutine!")
		return
	}
	close(s.cmdCache)
	//close stop channel
	close(s.stopCh)
	s.wg.Wait()
	s.srvImp.OnBeforeDestroy()
}

// addTask add stub call to service call queue
func (s *stubWrapper) addTask(task rpcTask) error {
	//check status
	if atomic.LoadInt32(&s.isClose) != 0 {
		s.logger.Warn("[Service] %s,%d,0 service has been shutdown while remote call", s.srvImp.GetServiceName(), s.srvImp.GetUUID())
		return errors.NewRpcError(errors.ServiceShutdown, "service %s has shutdown ", s.srvImp.GetServiceName())
	}

	//check channel
	if s.cmdCache == nil {
		return errors.NewRpcError(errors.ServiceShutdown, "service %s has shutdown ", s.srvImp.GetServiceName())
	}
	//add to cmdCache
	s.cmdCache <- task
	return nil
}

func (s *stubWrapper) isValid() bool {
	return atomic.LoadInt32(&s.isClose) == 0
}

func (s *stubWrapper) doCallService(trans transport.ITransport, stubCall *CallInfo) error {
	//check valid
	if !s.isValid() {
		return errors.ErrTransClose
	}

	task := &rpcCallTask{
		stubCall,
	}

	// add stub call to service
	err := s.addTask(task)
	if err != nil {
		return err
	}
	return nil
}
