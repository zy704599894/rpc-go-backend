package idlrpc

import "gitee.com/dennis-kk/rpc-go-backend/idlrpc/pkg/transport"

const (
	taskRpcCall = iota + 1
	taskSubCall
	taskCancelSub
)

type (
	rpcTask interface {
		Type() int
	}

	rpcCallTask struct {
		call *CallInfo
	}

	rpcSubTask struct {
		subId   string
		eName   string
		proxyId uint32
		data    []byte
		trans   transport.ITransport
	}

	rpcCancelTask struct {
		subId string
	}
)

func (r rpcCancelTask) Type() int {
	return taskCancelSub
}

func (r *rpcSubTask) Type() int {
	return taskSubCall
}

func (r *rpcCallTask) Type() int {
	return taskRpcCall
}
